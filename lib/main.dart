import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rantalcarweb/api/rentalCarApi.dart';
import 'package:rantalcarweb/bloc/authentication/authentication_bloc.dart';
import 'package:rantalcarweb/bloc/login/login_bloc.dart';
import 'package:rantalcarweb/repository/auth_repo.dart';
import 'package:rantalcarweb/repository/brand_repo.dart';
import 'package:rantalcarweb/src/features/brand/data/data_sources/brand_data_source.dart';
import 'package:rantalcarweb/src/features/brand/domain/use_cases/brand_use_case.dart';
import 'package:rantalcarweb/src/features/brand/presentation/bloc/brand_bloc.dart';
import 'package:rantalcarweb/src/shared/routes/routes.dart';
import 'package:rantalcarweb/src/shared/ui/layout/auth/auth_layout.dart';
import 'package:rantalcarweb/src/shared/ui/layout/dashboard/dashboard_layout.dart';
import 'package:rantalcarweb/src/shared/ui/layout/splash/splash_layout.dart';

import 'src/shared/ui/service/service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalStorage.configurePrefs();
  RentalCarApi.configureDio();
  Flurorouter.configureRoutes();
  runApp(const StateApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      scaffoldMessengerKey: NotificationService.messengerKey,
      navigatorKey: NavigationService.navigatorKey,
      title: 'Jepuru Car',
      initialRoute: '/',
      onGenerateRoute: Flurorouter.router.generator,
      builder: (_, child) {
        return BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {},
          builder: (context, state) {
            switch (state.status) {
              case AuthenticationStatus.authenticated:
                return DashboardLayout(child: child!);

              case AuthenticationStatus.checking:
                return const SplashLayout();

              case AuthenticationStatus.notAuthenticated:
                return AuthLayout(child: child!);
            }
          },
        );
      },
    );
  }
}

class StateApp extends StatelessWidget {
  const StateApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => Dio(
            BaseOptions(baseUrl: 'http://192.168.0.12:8080/api',
                // connectTimeout: 12000,
                // receiveTimeout: 12000,
                headers: {'x-token': LocalStorage.prefs.getString('token')}),
          ),
        ),
        RepositoryProvider(
          create: (context) => BrandDataSourcesImpl(dio: context.read<Dio>()),
        ),
      ],
      child: RepositoryProvider(
        create: (context) => BrandUseCase(
            brandDataSourcesImpl: context.read<BrandDataSourcesImpl>()),
        child: MultiBlocProvider(
            providers: [
              BlocProvider(create: (context) => AuthenticationBloc()),
              BlocProvider(
                create: (context) =>
                    LoginBloc(authRepository: AuthRepository()),
              ),
              BlocProvider(
                create: (context) =>
                    BrandBloc(brandUseCase: context.read<BrandUseCase>()),
              )
            ],
            child: MultiProvider(
              providers: [
                ChangeNotifierProvider(
                    lazy: false, create: (_) => SideMenuProvider()),
              ],
              child: const MyApp(),
            )),
      ),
    );
  }
}
