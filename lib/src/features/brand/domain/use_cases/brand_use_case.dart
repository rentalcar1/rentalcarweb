import 'package:rantalcarweb/src/features/brand/data/data_sources/brand_data_source.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mapper_response.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mappers.dart';

class BrandUseCase {
  final BrandDataSourcesImpl _brandDataSourcesImpl;

  BrandUseCase({required BrandDataSourcesImpl brandDataSourcesImpl})
      : _brandDataSourcesImpl = brandDataSourcesImpl;

  Future<List<BrandMapperResponse>> getAllBrand() async {
    return _brandDataSourcesImpl.getAllBrand();
  }

  Future<BrandMapperResponse> updateBrand(BrandMapper dto) async {
    return _brandDataSourcesImpl.updateBrand(dto: dto);
  }

  Future<BrandMapperResponse> createBrand(BrandMapper dto) async {
    return _brandDataSourcesImpl.createBrand(dto: dto);
  }

  Future<BrandMapperResponse> changeBrandState(String uid) async {
    return _brandDataSourcesImpl.changeBrandState(uid: uid);
  }
}
