import 'package:equatable/equatable.dart';

class BrandEntity extends Equatable {
  final String brandName;
  final String? brandImg;
  final bool status;
  final String uid;
  final String? base64Image;

  const BrandEntity(
      {this.brandImg,
      required this.brandName,
      required this.status,
      required this.uid,
      this.base64Image});
  @override
  List<Object?> get props => [brandName, brandImg, status, uid, base64Image];
}
