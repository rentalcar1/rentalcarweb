import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rantalcarweb/repository/datatables/brand_source.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mapper_response.dart';
import 'package:rantalcarweb/src/features/brand/presentation/widgets/brandModal.dart';
import 'package:rantalcarweb/src/shared/ui/buttons/custom_icon_button.dart';
import 'package:rantalcarweb/src/shared/ui/card/white_card.dart';
import 'package:rantalcarweb/src/shared/ui/service/service.dart';

import '../bloc/brand_bloc.dart';

class BrandView extends StatefulWidget {
  const BrandView({super.key});

  @override
  State<BrandView> createState() => _BrandViewState();
}

class _BrandViewState extends State<BrandView> {
  @override
  void initState() {
    BlocProvider.of<BrandBloc>(context, listen: false).add(GetAllBrandEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocConsumer<BrandBloc, BrandState>(
        listener: (context, state) {
          if (state.status == BrandStatus.error) {
            NotificationService.showDialogError(
                context: context, message: state.errorMessage!);
          }
        },
        builder: (context, state) {
          switch (state.status) {
            case BrandStatus.success:
            case BrandStatus.editMode:
            case BrandStatus.newBrand:
              return BrandDataView(
                data: state.brand!,
              );

            case BrandStatus.checking:
              return const Center(
                child: CircularProgressIndicator(),
              );

            case BrandStatus.error:
              return (state.errorMessage == null)
                  ? const Center(child: Text('Error'))
                  : BrandDataView(
                      data: state.brand!,
                    );
          }
        },
      ),
    );
  }
}

class BrandDataView extends StatelessWidget {
  final List<BrandMapperResponse> data;
  const BrandDataView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        child: ListView(
      physics: const ClampingScrollPhysics(),
      children: [
        WhiteCard(
            child: PaginatedDataTable(
          columns: const [
            DataColumn(label: Text('Img')),
            DataColumn(label: Text('Id')),
            DataColumn(label: Text('Marca')),
            DataColumn(label: Text('Estado')),
            DataColumn(label: Text('Acciones')),
          ],
          source: BrandSource(brands: data, context: context),
          /* onPageChanged: (value) {
            setState(() {
    _rowPerPage = value;
            });
          },
          rowsPerPage: _rowPerPage,*/
          rowsPerPage: 3,
          header: const Text(
            'Lista de todas las marcas',
            maxLines: 2,
          ),
          actions: [
            CustomIconButton(
                color: Colors.blueAccent,
                onPressed: () {
                  context.read<BrandBloc>().add(CreateNewBrandEvent());
                  showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (_) => BrandModal());
                },
                text: 'Agregar',
                icon: Icons.add),
          ],
        )),
      ],
    ));
  }
}
