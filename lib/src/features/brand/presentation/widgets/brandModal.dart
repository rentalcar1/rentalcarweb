import 'dart:convert';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rantalcarweb/repository/brand_repo.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mapper_response.dart';
import 'package:rantalcarweb/src/features/brand/presentation/bloc/brand_bloc.dart';
import 'package:rantalcarweb/src/shared/ui/card/white_card.dart';
import 'package:rantalcarweb/src/shared/ui/inputs/custom_inputs.dart';
import 'package:rantalcarweb/src/shared/ui/theme/app_color.dart';
import 'package:rantalcarweb/src/shared/ui/theme/text_style.dart';

class BrandModal extends StatelessWidget {
  final BrandMapperResponse? data;
  const BrandModal({super.key, this.data});

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    return Container(
      padding: const EdgeInsets.all(20),
      height: 650,
      width: 300,
      decoration: _buildBoxDecoration(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Editar Marca', style: AppStyles.h1),
              IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: const Icon(
                    Icons.close,
                    color: Colors.black87,
                  ))
            ],
          ),
          Divider(
            color: Colors.black.withOpacity(0.3),
          ),
          Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: {0: FixedColumnWidth(200)},
            children: [
              TableRow(children: [
                Brandimage(
                  data: data,
                ),
                if (_size.width >= 700)
                  TextFormField(
                    style:
                        AppStyles.titleNormal16.copyWith(color: Colors.black54),
                    initialValue: data?.brandName ?? '',
                    onChanged: (value) => context
                        .read<BrandBloc>()
                        .add(GetdataForUpdateEvent(brandName: value)),
                    decoration: CustomInputs.loginInputDecoration(
                        hint: 'Nombre de la marca',
                        etiqueta: 'Marca',
                        iconData: Icons.new_label,
                        borderColorSide: primaryLight,
                        hintStyle: true,
                        labelStyle: true,
                        iconColor: Colors.black54),
                  )
              ]),
              if (_size.width < 700)
                TableRow(children: [
                  TextFormField(
                    initialValue: data?.brandName ?? '',
                    onChanged: (value) => context
                        .read<BrandBloc>()
                        .add(GetdataForUpdateEvent(brandName: value)),
                    decoration: CustomInputs.loginInputDecoration(
                        hint: 'Nombre de la marca',
                        etiqueta: 'Marca',
                        iconData: Icons.new_label,
                        borderColorSide: primaryLight,
                        hintStyle: true,
                        labelStyle: true,
                        iconColor: Colors.black54),
                    style: AppStyles.titleNormal12,
                  )
                ])
            ],
          ),
          const SaveImageButton()
        ],
      ),
    );
  }

  BoxDecoration _buildBoxDecoration() => BoxDecoration(
      color: Colors.grey.shade100,
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)));
}

class SaveImageButton extends StatelessWidget {
  const SaveImageButton({super.key});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 110),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.indigo),
            shadowColor: MaterialStateProperty.all(Colors.transparent),
          ),
          child: Row(
            children: [Icon(Icons.save_outlined, size: 20), Text('  Guardar')],
          ),
          onPressed: () async {
            context.read<BrandBloc>().add(UpdateBrandDataEvent());
          },
        ));
  }
}

class Brandimage extends StatelessWidget {
  final BrandMapperResponse? data;
  const Brandimage({super.key, this.data});

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<BrandBloc>(context, listen: false);
    var imageAvatar = (data?.brandImg == null)
        ? const Image(image: AssetImage('assets/common/no-image.png'))
        : FadeInImage.assetNetwork(
            placeholder: 'assets/common/loader.gif', image: data!.brandImg);

    return BlocConsumer<BrandBloc, BrandState>(
      listener: (context, state) {
        if (state.status == BrandStatus.success) {
          Navigator.of(context).pop();
        }
      },
      builder: (context, state) {
        if (state.base64Image != null) {
          imageAvatar = Image.memory(
            convertBase64Image(state.base64Image!),
            gaplessPlayback: true,
          );
        }
        return WhiteCard(
            width: 250,
            child: SizedBox(
                width: double.infinity,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Imagen',
                        style: AppStyles.titleNormal16
                            .copyWith(color: Colors.black54),
                      ),
                      const SizedBox(width: 20),
                      SizedBox(
                        height: 150,
                        width: 160,
                        child: Stack(alignment: Alignment.center, children: [
                          imageAvatar,
                          Positioned(
                              bottom: 5,
                              right: 5,
                              child: Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    border: Border.all(
                                        color: Colors.white, width: 5)),
                                child: FloatingActionButton(
                                  elevation: 0,
                                  child: Icon(
                                    Icons.camera_alt_outlined,
                                    color: Colors.white,
                                  ),
                                  onPressed: (() async {
                                    FilePickerResult? result =
                                        await FilePicker.platform.pickFiles(
                                            allowMultiple: false,
                                            allowCompression: true);
                                    //NotificationService.showBusyIndicator(context);
                                    PlatformFile file = result!.files.first;
                                    final base64 = base64Encode(file.bytes!);
                                    bloc.add(GetdataForUpdateEvent(
                                        base64Image: base64));
                                  }),
                                ),
                              ))
                        ]),
                      )
                    ])));
      },
    );
  }

  Uint8List convertBase64Image(String base64String) {
    return const Base64Decoder().convert(base64String.split(',').last);
  }
}
