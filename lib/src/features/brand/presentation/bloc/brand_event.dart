part of 'brand_bloc.dart';

abstract class BrandEvent extends Equatable {
  const BrandEvent();

  @override
  List<Object> get props => [];
}

class GetAllBrandEvent extends BrandEvent {}

class GetSingleBrandEvent extends BrandEvent {
  final BrandMapperResponse brand;
  const GetSingleBrandEvent({required this.brand});
}

class GetdataForUpdateEvent extends BrandEvent {
  final String? brandName;
  final String? base64Image;
  const GetdataForUpdateEvent({this.brandName, this.base64Image});
}

class UpdateBrandDataEvent extends BrandEvent {}

class CreateNewBrandEvent extends BrandEvent {}

class ChangeBrandStatusEvent extends BrandEvent {}
