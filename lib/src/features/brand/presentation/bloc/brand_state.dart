part of 'brand_bloc.dart';

enum BrandStatus { checking, success, error, editMode, newBrand }

class BrandState extends Equatable {
  final List<BrandMapperResponse>? brand;
  final BrandMapperResponse? selectedBrand;
  final String? brandName;
  final String? base64Image;
  final String? errorMessage;
  final BrandStatus status;

  const BrandState(
      {this.brand,
      this.selectedBrand,
      this.errorMessage,
      required this.status,
      this.brandName,
      this.base64Image});

  BrandState copyWith(
          {List<BrandMapperResponse>? brand,
          required BrandStatus status,
          String? brandName,
          String? base64Image,
          String? errorMessage,
          BrandMapperResponse? selectedBrand}) =>
      BrandState(
          brand: brand ?? this.brand,
          status: status,
          errorMessage: errorMessage ?? this.errorMessage,
          base64Image: base64Image ?? this.base64Image,
          brandName: brandName ?? this.brandName,
          selectedBrand: selectedBrand ?? this.selectedBrand);
  @override
  List<Object?> get props =>
      [brand, status, selectedBrand, brandName, base64Image, errorMessage];
}
