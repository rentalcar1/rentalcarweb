import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:provider/provider.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mapper_response.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mappers.dart';
import 'package:rantalcarweb/src/features/brand/domain/use_cases/brand_use_case.dart';
import 'package:rantalcarweb/src/shared/errors/api_exceptions.dart';

part 'brand_event.dart';
part 'brand_state.dart';

class BrandBloc extends Bloc<BrandEvent, BrandState> {
  BrandBloc({required BrandUseCase brandUseCase})
      : _brandUseCase = brandUseCase,
        super(const BrandState(status: BrandStatus.checking)) {
    on<GetAllBrandEvent>(_getAllBrandEvent);
    on<GetSingleBrandEvent>(_getSingleBrand);
    on<GetdataForUpdateEvent>(_getDataForUpdate);
    on<UpdateBrandDataEvent>(_updateBrandData);
    on<CreateNewBrandEvent>(_createNewBrand);
    on<ChangeBrandStatusEvent>(_changeBrandStatus);
  }
  final BrandUseCase _brandUseCase;
  void _getAllBrandEvent(
      GetAllBrandEvent event, Emitter<BrandState> emit) async {
    emit(state.copyWith(status: BrandStatus.checking));

    final resp = await _brandUseCase.getAllBrand();

    emit(state.copyWith(brand: resp, status: BrandStatus.success));
  }

  void _getSingleBrand(GetSingleBrandEvent event, Emitter<BrandState> emit) {
    emit(state.copyWith(
        status: BrandStatus.editMode, selectedBrand: event.brand));
  }

  void _getDataForUpdate(
      GetdataForUpdateEvent event, Emitter<BrandState> emit) {
    emit(state.copyWith(
        status: state.status,
        base64Image: event.base64Image ?? state.base64Image,
        brandName: event.brandName ?? state.brandName));
  }

  void _updateBrandData(
      UpdateBrandDataEvent event, Emitter<BrandState> emit) async {
    if (state.status != BrandStatus.newBrand) {
      BrandMapper dto = BrandMapper(
          base64Image: state.base64Image,
          brandName: state.brandName ?? state.selectedBrand!.brandName,
          status: state.selectedBrand!.status,
          uid: state.selectedBrand!.uid);

      final resp = await _brandUseCase.updateBrand(dto);
      state.brand!.remove(state.selectedBrand);
      final brand = state.brand;
      brand!.add(resp);
      emit(state.copyWith(
          status: BrandStatus.success,
          brand: brand,
          base64Image: null,
          brandName: null,
          selectedBrand: null));
    } else {
      if (state.base64Image == null) {
        emit(state.copyWith(
            status: BrandStatus.error,
            errorMessage: 'No ha seleccionado ninguna Imagen.'));
      } else if (state.brandName == null) {
        emit(state.copyWith(
            status: BrandStatus.error,
            errorMessage: 'El nombre de la marca no puede ser nulo.'));
      } else {
        BrandMapper dto = BrandMapper(
            base64Image: state.base64Image,
            brandName: state.brandName!,
            status: true,
            uid: '0');
        try {
          final resp = await _brandUseCase.createBrand(dto);
          final data = state.brand;
          data!.add(resp);
          emit(state.copyWith(
              status: BrandStatus.success,
              base64Image: null,
              errorMessage: null,
              brand: data));
        } on ApiServerException catch (error) {
          emit(state.copyWith(
              status: BrandStatus.error, errorMessage: error.errorMessage));
        }
      }
    }
  }

  void _createNewBrand(CreateNewBrandEvent event, Emitter<BrandState> emit) {
    emit(state.copyWith(status: BrandStatus.newBrand));
  }

  void _changeBrandStatus(
      ChangeBrandStatusEvent event, Emitter<BrandState> emit) async {
    try {
      final resp =
          await _brandUseCase.changeBrandState(state.selectedBrand!.uid);
      state.brand!.remove(state.selectedBrand);
      final brand = state.brand;
      brand!.add(resp);
      emit(state.copyWith(
          status: BrandStatus.success,
          brand: brand,
          base64Image: null,
          brandName: null,
          selectedBrand: null));
    } on ApiServerException catch (error) {
      emit(state.copyWith(
          status: BrandStatus.error, errorMessage: error.errorMessage));
    } catch (e) {
      emit(state.copyWith(
          status: BrandStatus.error, errorMessage: e.toString()));
    }
  }
}
