import 'package:dio/dio.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mapper_response.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mappers.dart';
import 'package:rantalcarweb/src/shared/errors/api_exceptions.dart';

abstract class BrandDataSources {
  Future<BrandMapperResponse> createBrand({required BrandMapper dto});
  Future<List<BrandMapperResponse>> getAllBrand();
  Future<BrandMapperResponse> updateBrand({required BrandMapper dto});
  Future<BrandMapperResponse> changeBrandState({required String uid});
}

class BrandDataSourcesImpl extends BrandDataSources {
  final Dio _dio;
  BrandDataSourcesImpl({required Dio dio}) : _dio = dio;
  @override
  Future<List<BrandMapperResponse>> getAllBrand() async {
    const endpoint = '/brand';
    try {
      final resp = await _dio.get(endpoint);
      final respuesta =
          resp.data.map((e) => BrandMapperResponse.fromJson(e)).toList();
      return List<BrandMapperResponse>.from(respuesta);
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response != null) {
          if (e.response!.statusCode == 400) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 401) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 500) {
            throw ApiServerException.fromJson(e.response!.data);
          }
        } else {
          throw ApiServerException.fromJson(
              {'codError': -98, 'errorMessage': 'Error Interno'});
        }
      }
      throw ApiServerException.fromJson(
          {'codError': -99, 'errorMessage': 'Error Interno'});
    }
  }

  @override
  Future<BrandMapperResponse> updateBrand({required BrandMapper dto}) async {
    const endpoint = '/brand/upload';
    final data = dto.toJson();
    try {
      final resp = await _dio.post(endpoint, data: data);
      return BrandMapperResponse.fromJson(resp.data);
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response != null) {
          if (e.response!.statusCode == 400) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 401) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 500) {
            throw ApiServerException.fromJson(e.response!.data);
          }
        } else {
          throw ApiServerException.fromJson(
              {'codError': -98, 'errorMessage': 'Error Interno'});
        }
      }
    }
    throw ApiServerException.fromJson(
        {'codError': -99, 'errorMessage': 'Error Interno'});
  }

  @override
  Future<BrandMapperResponse> createBrand({required BrandMapper dto}) async {
    const endpoint = '/brand';
    final data = dto.toJson();
    try {
      final resp = await _dio.post(endpoint, data: data);
      return BrandMapperResponse.fromJson(resp.data);
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response != null) {
          if (e.response!.statusCode == 400) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 401) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 410) {
            throw ApiServerException.fromJson(e.response!.data);
          }
          if (e.response!.statusCode == 500) {
            throw ApiServerException.fromJson(e.response!.data);
          }
        } else {
          throw ApiServerException.fromJson(
              {'codError': -98, 'errorMessage': 'Error Interno'});
        }
      }
    }
    throw ApiServerException.fromJson(
        {'codError': -99, 'errorMessage': 'Error Interno'});
  }

  @override
  Future<BrandMapperResponse> changeBrandState({required String uid}) async {
    try {
      final resp = await _dio.post('/brand/status', data: {'uid': uid});
      return BrandMapperResponse.fromJson(resp.data);
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response!.statusCode == 400) {
          throw ApiServerException.fromJson(e.response!.data);
        }
        if (e.response!.statusCode == 401) {
          throw ApiServerException.fromJson(e.response!.data);
        }
        if (e.response!.statusCode == 404) {
          throw ApiServerException.fromJson(
              {'codError': 404, 'errorMessage': 'Metodo inexistente.'});
        }
        if (e.response!.statusCode == 410) {
          throw ApiServerException.fromJson(e.response!.data);
        }
        if (e.response!.statusCode == 500) {
          throw ApiServerException.fromJson(e.response!.data);
        }
      } else {
        throw ApiServerException.fromJson(
            {'codError': -98, 'errorMessage': 'Error Interno'});
      }
      throw ApiServerException.fromJson(
          {'codError': -99, 'errorMessage': 'Error Interno'});
    }
  }
}
