import 'package:rantalcarweb/src/features/brand/domain/entities/brand_entity.dart';

class BrandMapper extends BrandEntity {
  const BrandMapper(
      {required String brandName,
      String? brandImg,
      required bool status,
      required String uid,
      String? base64Image})
      : super(
            base64Image: base64Image,
            brandImg: brandImg,
            brandName: brandName,
            status: status,
            uid: uid);

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'brandName': brandName,
      'brandImg': brandImg,
      'status': status,
      'uid': uid,
      'base64Image': base64Image
    };
  }

  factory BrandMapper.fromJson(Map<String, dynamic> map) {
    return BrandMapper(
        brandName: map['brandName'],
        brandImg: map['brandImg'],
        status: map['status'],
        base64Image: map['base64Image'],
        uid: map['uid']);
  }
}
