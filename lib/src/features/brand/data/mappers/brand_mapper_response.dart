import 'dart:convert';

List<BrandMapperResponse> brandMapperResponseFromJson(String str) =>
    List<BrandMapperResponse>.from(
        json.decode(str).map((x) => BrandMapperResponse.fromJson(x)));

String brandMapperResponseToJson(List<BrandMapperResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BrandMapperResponse {
  BrandMapperResponse({
    required this.brandName,
    required this.brandImg,
    required this.status,
    required this.uid,
  });

  final String brandName;
  final String brandImg;
  final bool status;
  final String uid;

  factory BrandMapperResponse.fromJson(Map<String, dynamic> json) =>
      BrandMapperResponse(
        brandName: json["brandName"],
        brandImg: json["brandImg"],
        status: json["status"],
        uid: json["uid"],
      );

  Map<String, dynamic> toJson() => {
        "brandName": brandName,
        "brandImg": brandImg,
        "status": status,
        "uid": uid,
      };
}
