import 'package:flutter/material.dart';
import 'package:rantalcarweb/src/shared/ui/service/service.dart';
import 'package:rantalcarweb/src/shared/ui/shared/side_bar.dart';

class DashboardLayout extends StatefulWidget {
  final Widget child;
  const DashboardLayout({super.key, required this.child});

  @override
  State<DashboardLayout> createState() => _DashboardLayoutState();
}

class _DashboardLayoutState extends State<DashboardLayout>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    SideMenuProvider.menuController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: [
        Row(
          children: [
            if (size.width >= 700) const SideBar(),
            Expanded(
                child: Column(
              children: [
                //todo: navbar
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Center(
                        child: widget.child,
                      )),
                ),
              ],
            ))
          ],
        )
      ],
    ));
  }
}
