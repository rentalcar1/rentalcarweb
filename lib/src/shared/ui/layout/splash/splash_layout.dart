import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rantalcarweb/bloc/authentication/authentication_bloc.dart';
import 'package:rantalcarweb/src/shared/ui/service/local_storage.dart';

class SplashLayout extends StatefulWidget {
  const SplashLayout({Key? key}) : super(key: key);

  @override
  State<SplashLayout> createState() => _SplashLayoutState();
}

class _SplashLayoutState extends State<SplashLayout> {
  @override
  void initState() {
    context.read<AuthenticationBloc>().add(const CheckAuthEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            SizedBox(
              height: 20,
            ),
            Text('Checking...')
          ],
        ),
      ),
    );
  }
}
