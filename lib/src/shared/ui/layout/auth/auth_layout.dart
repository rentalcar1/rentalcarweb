import 'package:flutter/material.dart';

import '../../theme/theme.dart';

class AuthLayout extends StatelessWidget {
  final Widget child;
  const AuthLayout({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: primaryColor,
        body: SizedBox(
          height: size.height,
          width: size.width,
          child: Row(
            children: [
              Image.asset('assets/common/logonew.jpg'),
              SizedBox(
                width: size.width * 0.6,
                height: size.height * 0.6,
                child: child,
              ),
            ],
          ),
        ));
  }
}
