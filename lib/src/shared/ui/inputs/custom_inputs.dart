import 'package:flutter/material.dart';
import 'package:rantalcarweb/src/shared/ui/theme/theme.dart';

class CustomInputs {
  static InputDecoration searchInputDecoration(
      {required String hint, required IconData icon}) {
    return InputDecoration(
        border: InputBorder.none,
        enabledBorder: InputBorder.none,
        hintText: hint,
        labelStyle: TextStyle(color: Colors.grey),
        hintStyle: TextStyle(color: Colors.grey),
        icon: Icon(
          icon,
          color: Colors.grey,
        ));
  }

  static InputDecoration loginInputDecoration(
          {required String hint,
          required String etiqueta,
          required IconData iconData,
          Color borderColorSide = Colors.black,
          bool hintStyle = false,
          bool labelStyle = false,
          Color iconColor = Colors.blue}) =>
      InputDecoration(
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
          color: borderColorSide,
        )),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
          color: borderColorSide,
        )),
        border: OutlineInputBorder(
            borderSide: BorderSide(
          color: borderColorSide,
        )),
        hintText: hint,
        focusColor: Colors.black,
        hintStyle: (hintStyle)
            ? AppStyles.titleNormal16.copyWith(color: Colors.black54)
            : null,
        labelText: etiqueta,
        labelStyle: (labelStyle)
            ? AppStyles.titleNormal16.copyWith(color: Colors.black54)
            : null,
        prefixIcon: Icon(
          iconData,
          color: iconColor,
        ),
      );

  static InputDecoration formInputDecoration({
    required String hint,
    required String label,
    required IconData icon,
  }) {
    return InputDecoration(
      border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.indigo.withOpacity(0.3))),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.indigo.withOpacity(0.3))),
      hintText: hint,
      labelText: label,
      prefixIcon: Icon(icon, color: Colors.grey),
      labelStyle: TextStyle(color: Colors.grey),
      hintStyle: TextStyle(color: Colors.grey),
    );
  }
}
