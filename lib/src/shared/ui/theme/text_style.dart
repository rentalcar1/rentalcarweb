import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppStyles {
  static TextStyle h1 =
      GoogleFonts.roboto(fontSize: 30, fontWeight: FontWeight.w400);
  static TextStyle h2 =
      GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400);
  static TextStyle titleNormal12 =
      GoogleFonts.roboto(color: Colors.white, fontSize: 12);
  static TextStyle titleNormal14 =
      GoogleFonts.roboto(color: Colors.white, fontSize: 14);
  static TextStyle titleNormal16 =
      GoogleFonts.roboto(color: Colors.white, fontSize: 16);
  static TextStyle titleNormal25 = GoogleFonts.roboto(fontSize: 25);
}
