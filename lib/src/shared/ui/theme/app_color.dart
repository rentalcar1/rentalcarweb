import 'package:flutter/material.dart';

//const Color primaryColor = Color.fromARGB(255, 44, 112, 214);
const primaryColor = Color.fromRGBO(247, 107, 22, 1);
const primaryLight = Color.fromRGBO(255, 156, 73, 1);
const primaryDarck = Color.fromRGBO(189, 58, 0, 1);
const secundary = Color.fromRGBO(189, 58, 0, 1);
const dashboardChildColor = Color(0xffedf1f2);
