import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rantalcarweb/bloc/authentication/authentication_bloc.dart';
import 'package:rantalcarweb/src/shared/routes/routes.dart';
import 'package:rantalcarweb/src/shared/ui/shared/item_menu.dart';
import 'package:rantalcarweb/src/shared/ui/shared/text_separator.dart';
import 'package:rantalcarweb/src/shared/ui/theme/app_color.dart';

import '../service/service.dart';

class SideBar extends StatelessWidget {
  const SideBar({super.key});
  void navigateTo(String routeName) {
    NavigationService.navigateTo(routeName);
    SideMenuProvider.closeMenu();
  }

  @override
  Widget build(BuildContext context) {
    final sideMenuProvider = Provider.of<SideMenuProvider>(context);
    return Container(
      width: 200,
      height: MediaQuery.of(context).size.height,
      decoration: _boxDecoration(),
      child: Column(
        children: [
          Expanded(
            child: ListView(
              physics: const ClampingScrollPhysics(),
              children: [
                const TextSeparator(text: 'Main'),
                ItemMenu(
                    text: 'Dashboard',
                    icon: Icons.compass_calibration_outlined,
                    isActived: sideMenuProvider.currentPage ==
                            Flurorouter.dashboardRoute
                        ? true
                        : false,
                    onPressed: () => navigateTo(Flurorouter.dashboardRoute)),
                ItemMenu(
                    text: 'Marcas',
                    icon: Icons.card_membership_outlined,
                    isActived:
                        sideMenuProvider.currentPage == Flurorouter.brandRoute
                            ? true
                            : false,
                    onPressed: () => navigateTo(Flurorouter.brandRoute))
              ],
            ),
          ),
          ItemMenu(
              text: 'Cerrar',
              icon: Icons.logout_rounded,
              isActived: false,
              onPressed: () =>
                  context.read<AuthenticationBloc>().add(LogOutEvent())),
        ],
      ),
    );
  }

  BoxDecoration _boxDecoration() => const BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 5,
          offset: Offset(0, 0),
        )
      ], gradient: LinearGradient(colors: [primaryColor, primaryLight]));
}
