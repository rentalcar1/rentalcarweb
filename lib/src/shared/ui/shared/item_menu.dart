import 'package:flutter/material.dart';
import 'package:rantalcarweb/src/shared/ui/theme/theme.dart';

class ItemMenu extends StatefulWidget {
  final String text;
  final IconData icon;
  final bool isActived;
  final Function onPressed;
  const ItemMenu(
      {Key? key,
      required this.text,
      required this.icon,
      this.isActived = false,
      required this.onPressed})
      : super(key: key);

  @override
  State<ItemMenu> createState() => _ItemMenuState();
}

class _ItemMenuState extends State<ItemMenu> {
  bool isHovered = false;
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 150),
      color: (isHovered)
          ? Colors.white.withOpacity(0.2)
          : (widget.isActived)
              ? Colors.white.withOpacity(0.2)
              : Colors.transparent,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: widget.isActived ? null : (() => widget.onPressed()),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 5),
            child: MouseRegion(
              onEnter: (_) => setState(() => isHovered = true),
              onExit: (_) => setState(() => isHovered = false),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    widget.icon,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(widget.text, style: AppStyles.titleNormal16)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
