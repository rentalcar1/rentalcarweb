import 'package:flutter/material.dart';
import 'package:rantalcarweb/src/shared/ui/theme/text_style.dart';

import '../../card/white_card.dart';

class DashboardView extends StatelessWidget {
  const DashboardView({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
      physics: const ClampingScrollPhysics(),
      children: [
        Text(
          'Dashboard view',
          style: AppStyles.h1,
        ),
        const WhiteCard(title: 'Sales Statistics', child: Text('Hola Mundo'))
      ],
    ));
  }
}
