import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rantalcarweb/bloc/authentication/authentication_bloc.dart';
import 'package:rantalcarweb/bloc/login/login_bloc.dart';
import 'package:rantalcarweb/src/shared/ui/service/local_storage.dart';
import 'package:rantalcarweb/src/shared/ui/service/notificacion_service.dart';
import 'package:rantalcarweb/src/shared/ui/buttons/custom_outline_button.dart';
import 'package:rantalcarweb/src/shared/ui/theme/theme.dart';

import '../../buttons/link_text.dart';

class LoginView extends StatelessWidget {
  const LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        print('login listener status ${state}');
        switch (state.status) {
          case LoginStatus.connected:
            context.read<AuthenticationBloc>().add(
                CheckAuthEvent(token: LocalStorage.prefs.getString('token')!));
            break;
          case LoginStatus.noConnected:
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(content: Text(state.apiMessage!)),
              );
            break;
          default:
        }
      },
      builder: (context, state) {
        return SizedBox(
          width: size.width,
          child: Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400),
              child: Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                width: size.width,
                decoration: _boxDecoration(),
                child: Column(
                  children: [
                    Flexible(flex: 1, child: Container()),
                    Text(
                      'Iniciar Sesion',
                      style: AppStyles.titleNormal25,
                    ),
                    Flexible(flex: 1, child: Container()),
                    EmailField(state: state),
                    const SizedBox(height: 20),
                    PasswordField(state: state),
                    Flexible(flex: 1, child: Container()),
                    CustomOutlinedButton(
                        onPressed: () {
                          context.read<LoginBloc>().add(SubmitLoginEvent());
                          /*final bloc =
                              BlocProvider.of<AuthenticationBloc>(context);*/
                          //print(bloc.state.status);
                          context.read<AuthenticationBloc>().add(CheckAuthEvent(
                              token: LocalStorage.prefs.getString('token')!));
                        },
                        text: 'Ingresar'),
                    Flexible(flex: 1, child: Container()),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('¿No tienes una cuenta?'),
                        const SizedBox(width: 10),
                        LinkText(
                          text: 'Registrate',
                          onPressed: () {
                            print('navegar al registro');
                          },
                        ),
                      ],
                    ),
                    Flexible(flex: 1, child: Container()),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  BoxDecoration _boxDecoration() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.1),
              offset: Offset(0, 5),
              blurRadius: 10)
        ]);
  }
}

class EmailField extends StatelessWidget {
  final LoginState state;
  const EmailField({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        initialValue: state.email,
        onChanged: ((value) =>
            context.read<LoginBloc>().add(EmailFieldCheckEvent(email: value))),
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.indigo.withOpacity(0.3))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.indigo.withOpacity(0.3))),
          hintText: "ejemplo@empresa.com",
          labelText: "Email",
          errorText: (state.emailStatus == LoginStatus.noEmail)
              ? 'Debe ingresar su correo'
              : null,
          prefixIcon: const Icon(Icons.lock_clock_rounded, color: Colors.grey),
          labelStyle: const TextStyle(color: Colors.grey),
          hintStyle: const TextStyle(color: Colors.grey),
        ));
  }
}

class PasswordField extends StatelessWidget {
  final LoginState state;
  const PasswordField({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        initialValue: state.password,
        onChanged: (value) => context
            .read<LoginBloc>()
            .add(PasswordFieldCheckEvent(password: value)),
        obscureText: true,
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.indigo.withOpacity(0.3))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.indigo.withOpacity(0.3))),
          hintText: "Ingrese su contraseña",
          labelText: "Contraseña",
          errorText: (state.passwordStatus == LoginStatus.noPassword)
              ? 'Debe ingresar su contraseña'
              : null,
          prefixIcon: const Icon(Icons.lock_clock_rounded, color: Colors.grey),
          labelStyle: const TextStyle(color: Colors.grey),
          hintStyle: const TextStyle(color: Colors.grey),
        ));
  }
}
