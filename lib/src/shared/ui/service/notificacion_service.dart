import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

class NotificationService {
  static GlobalKey<ScaffoldMessengerState> messengerKey =
      GlobalKey<ScaffoldMessengerState>();

  static showSnackBarError(
      {String? message, Color color = Colors.red, required String mensaje}) {
    final snackBar = SnackBar(
        backgroundColor: color.withOpacity(0.9),
        content: Text(
          message!,
          style: const TextStyle(color: Colors.white, fontSize: 20),
        ));
    messengerKey.currentState!.showSnackBar(snackBar);
  }

  static showBusyIndicator(BuildContext context) {
    final AlertDialog dialog = AlertDialog(
      content: Container(
        decoration: const BoxDecoration(color: Colors.transparent),
        width: 100,
        height: 100,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              SizedBox(
                height: 15,
              ),
              Text('Iniciando sesion...'),
            ],
          ),
        ),
      ),
    );

    showDialog(context: context, builder: (_) => dialog);
  }

  static showDialogError(
      {required BuildContext context, required String message}) {
    AwesomeDialog(
      width: 650,
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.scale,
      body: Center(
        child: Text(
          message,
          style: const TextStyle(fontStyle: FontStyle.normal, fontSize: 25),
        ),
      ),
      btnOkOnPress: () {},
    ).show();
  }
}
