class ApiServerException {
  final String codeError;
  final String errorMessage;
  ApiServerException({required this.codeError, required this.errorMessage});
  factory ApiServerException.fromJson(Map<String, dynamic> json) =>
      ApiServerException(
          codeError: json["codeError"], errorMessage: json["errorMessage"]);

  Map<String, dynamic> toJson() =>
      {"codeError": codeError, "errorMessage": errorMessage};
}
