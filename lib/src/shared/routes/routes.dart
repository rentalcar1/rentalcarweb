import 'package:fluro/fluro.dart';

import 'admin_handlers.dart';
import 'dashboard_handlers.dart';

class Flurorouter {
  static final FluroRouter router = FluroRouter();

  static String rootRoute = '/';

  // Auth Router
  static String loginRoute = '/auth/login';
  static String registerRoute = '/auth/register';

  // Dashboard
  static String dashboardRoute = '/dashboard';
  static String brandRoute = '/dashboard/brand';

  static void configureRoutes() {
    // Auth Routes
    router.define(rootRoute,
        handler: AdminHandlers.login, transitionType: TransitionType.none);
    router.define(loginRoute,
        handler: AdminHandlers.login, transitionType: TransitionType.none);
    /*router.define(registerRoute,
        handler: AdminHandlers.register, transitionType: TransitionType.none);*/

    // Dashboard
    router.define(dashboardRoute,
        handler: DashboardHandlers.dashboard,
        transitionType: TransitionType.fadeIn);
    router.define(brandRoute,
        handler: DashboardHandlers.brand,
        transitionType: TransitionType.fadeIn);
  }
}
