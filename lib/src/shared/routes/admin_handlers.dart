import 'package:fluro/fluro.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rantalcarweb/bloc/authentication/authentication_bloc.dart';
import 'package:rantalcarweb/src/shared/ui/views/dashboard/dashboard_view.dart';
import 'package:rantalcarweb/src/shared/ui/views/login/login_view.dart';

class AdminHandlers {
  static Handler login = Handler(handlerFunc: (context, params) {
    final authBloc = BlocProvider.of<AuthenticationBloc>(context!);

    if (authBloc.state.status == AuthenticationStatus.notAuthenticated) {
      return const LoginView();
    } else {
      return const DashboardView();
    }
  });
}
