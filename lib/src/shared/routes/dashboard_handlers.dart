import 'package:fluro/fluro.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rantalcarweb/bloc/authentication/authentication_bloc.dart';
import 'package:rantalcarweb/src/shared/routes/routes.dart';
import 'package:rantalcarweb/src/shared/ui/service/service.dart';
import 'package:rantalcarweb/src/features/brand/presentation/screen/brand_view.dart';
import 'package:rantalcarweb/src/shared/ui/views/login/login_view.dart';

import '../ui/views/dashboard/dashboard_view.dart';

class DashboardHandlers {
  static Handler dashboard = Handler(handlerFunc: (context, params) {
    Provider.of<SideMenuProvider>(context!, listen: false)
        .setCurrentPageUrl(Flurorouter.dashboardRoute);

    final bloc = BlocProvider.of<AuthenticationBloc>(context);

    if (bloc.state.status == AuthenticationStatus.authenticated) {
      return const DashboardView();
    } else {
      return const LoginView();
    }
  });
  static Handler brand = Handler(handlerFunc: (context, params) {
    final bloc = BlocProvider.of<AuthenticationBloc>(context!);
    Provider.of<SideMenuProvider>(context, listen: false)
        .setCurrentPageUrl(Flurorouter.brandRoute);
    if (bloc.state.status == AuthenticationStatus.authenticated) {
      return const BrandView();
    } else {
      return const LoginView();
    }
  });
}
