import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:rantalcarweb/src/shared/errors/api_exceptions.dart';

import '../src/shared/ui/service/service.dart';

class RentalCarApi {
  static final Dio _dio = Dio();

  static void configureDio() {
    // Base del url
    _dio.options.baseUrl = 'http://192.168.0.12:8080/api';

    // Configurar Headers
    _dio.options.headers = {'x-token': LocalStorage.prefs.getString('token')};
  }

  static Future httpGet({required String path}) async {
    debugPrint('token generado ${LocalStorage.prefs.getString('token')}');
    _dio.options.headers = {'x-token': LocalStorage.prefs.getString('token')};
    final resp = await _dio.get(path);

    return resp.data;
  }

  static Future post(
      {required String path, required Map<String, dynamic> data}) async {
    final formData = FormData.fromMap(data);

    try {
      final resp = await _dio.post(path, data: formData);
      return resp;
    } on DioError catch (e) {
      if (e.response != null) {
        return ApiServerException.fromJson(e.response!.data);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        print(e.requestOptions);
        print(e.message);
      }
    }
  }

  static Future postImage(
      {required String path,
      required String base64,
      String? fileName,
      required String uid}) async {
    final formData =
        FormData.fromMap({'uid': uid, 'filename': fileName, 'base64': base64});

    try {
      final resp = await _dio.post(path, data: formData);
      return resp;
    } on DioError catch (e) {
      if (e.response != null) {
        return ApiServerException.fromJson(e.response!.data);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        print(e.requestOptions.data);
        print(e.message);
      }
    }
  }

  static Future postwithNoData({required String path}) async {
    try {
      final resp = await _dio.post(path);
      return resp;
    } on DioError catch (e) {
      if (e.response != null) {
        return ApiServerException.fromJson(e.response!.data);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        print(e.requestOptions);
        print(e.message);
      }
    }
  }
}
