import 'package:rantalcarweb/api/rentalCarApi.dart';

class AuthRepository {
  Future<dynamic> logIn({
    required String email,
    required String password,
  }) async {
    final resp = await RentalCarApi.post(
        path: '/auth/login', data: {'email': email, 'password': password});

    return resp;
  }

  void logOut() {}
}
