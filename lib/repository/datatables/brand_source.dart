import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rantalcarweb/src/features/brand/data/mappers/brand_mapper_response.dart';
import 'package:rantalcarweb/src/features/brand/presentation/bloc/brand_bloc.dart';
import 'package:rantalcarweb/src/features/brand/presentation/widgets/brandModal.dart';

class BrandSource extends DataTableSource {
  final List<BrandMapperResponse> brands;
  final BuildContext context;

  BrandSource({required this.brands, required this.context});
  @override
  DataRow? getRow(int index) {
    return DataRow.byIndex(index: index, cells: [
      DataCell(FadeInImage(
        height: 40,
        imageErrorBuilder: (context, error, stackTrace) =>
            const Image(image: AssetImage('assets/common/no-image.png')),
        placeholder: const AssetImage('assets/common/loader.gif'),
        image: NetworkImage(
          brands[index].brandImg,
        ),
      )),
      DataCell(Text(brands[index].uid.toString())),
      DataCell(Text(brands[index].brandName)),
      DataCell(Text(brands[index].status ? 'Activo' : 'Inactivo')),
      DataCell(Row(
        children: [
          IconButton(
            icon: Icon(
              Icons.edit,
              color: Colors.green.shade300,
            ),
            onPressed: () {
              context
                  .read<BrandBloc>()
                  .add(GetSingleBrandEvent(brand: brands[index]));
              showModalBottomSheet(
                  backgroundColor: Colors.transparent,
                  context: context,
                  builder: (_) => BrandModal(
                        data: brands[index],
                      ));
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.delete,
              color: Colors.red,
            ),
            onPressed: () {
              context
                  .read<BrandBloc>()
                  .add(GetSingleBrandEvent(brand: brands[index]));
              context.read<BrandBloc>().add(ChangeBrandStatusEvent());
            },
          ),
        ],
      )),
    ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => brands.length;

  @override
  int get selectedRowCount => 0;
}
