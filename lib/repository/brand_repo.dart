import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:rantalcarweb/api/rentalCarApi.dart';

class BrandRepository {
  Future<dynamic> getBrand() async {
    final resp = await RentalCarApi.httpGet(path: '/brand');
    return resp;
  }

  Future<dynamic> uploadBrandImage(
      {required String uid, String? fileName, required String base64}) async {
    final resp = await RentalCarApi.postImage(
        path: '/brand/upload', uid: uid, fileName: fileName, base64: base64);
    return resp;
  }
}
