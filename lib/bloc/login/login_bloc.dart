import 'package:bloc/bloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:equatable/equatable.dart';
import 'package:rantalcarweb/src/shared/errors/api_exceptions.dart';
import 'package:rantalcarweb/src/shared/ui/service/local_storage.dart';

import '../../repository/auth_repo.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({required AuthRepository authRepository})
      : _authRepository = authRepository,
        super(const LoginState()) {
    on<EmailFieldCheckEvent>(_onEmailFieldEvent);
    on<PasswordFieldCheckEvent>(_onPasswordEvent);
    on<SubmitLoginEvent>(_onSummit);
  }
  final AuthRepository _authRepository;
  void _onEmailFieldEvent(
      EmailFieldCheckEvent event, Emitter<LoginState> emit) {
    bool check = EmailValidator.validate(event.email);
    if (check) {
      emit(state.copyWith(
          email: event.email, emailStatus: LoginStatus.withEmail));
    }
  }

  void _onPasswordEvent(
      PasswordFieldCheckEvent event, Emitter<LoginState> emit) {
    if (event.password.isNotEmpty) {
      emit(state.copyWith(
          password: event.password, passwordStatus: LoginStatus.withPassword));
    }
  }

  void _onSummit(SubmitLoginEvent event, Emitter<LoginState> emit) async {
    if (state.email == null || state.email!.isEmpty) {
      emit(state.copyWith(emailStatus: LoginStatus.noEmail));
    }

    if (state.password == null || state.password!.isEmpty) {
      emit(state.copyWith(passwordStatus: LoginStatus.noPassword));
    }

    final resp = await _authRepository.logIn(
        email: state.email!, password: state.password!);
    if (resp is ApiServerException) {
      String message = resp.errorMessage;
      emit(
          state.copyWith(status: LoginStatus.noConnected, apiMessage: message));
    } else {
      LocalStorage.prefs.setString('token', resp.data['token']);
      emit(state.copyWith(status: LoginStatus.connected));
    }
  }
}
