part of 'login_bloc.dart';

enum LoginStatus {
  noPassword,
  noEmail,
  withEmail,
  withPassword,
  connected,
  noConnected
}

class LoginState extends Equatable {
  final String? email;
  final String? password;
  final LoginStatus? passwordStatus;
  final LoginStatus? emailStatus;
  final LoginStatus? status;
  final String? apiMessage;
  const LoginState(
      {this.email,
      this.password,
      this.emailStatus,
      this.passwordStatus,
      this.status,
      this.apiMessage});

  LoginState copyWith(
          {String? email,
          String? password,
          LoginStatus? emailStatus,
          LoginStatus? passwordStatus,
          LoginStatus? status,
          String? apiMessage}) =>
      LoginState(
          email: email ?? this.email,
          password: password ?? this.password,
          emailStatus: emailStatus ?? this.emailStatus,
          passwordStatus: passwordStatus ?? this.passwordStatus,
          status: status ?? this.status,
          apiMessage: apiMessage ?? this.apiMessage);
  @override
  List<Object?> get props =>
      [email, password, passwordStatus, emailStatus, status, apiMessage];
}
