part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class EmailFieldCheckEvent extends LoginEvent {
  final String email;
  const EmailFieldCheckEvent({required this.email});
}

class PasswordFieldCheckEvent extends LoginEvent {
  final String password;
  const PasswordFieldCheckEvent({required this.password});
}

class SubmitLoginEvent extends LoginEvent {}
