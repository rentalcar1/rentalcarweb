part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class CheckAuthEvent extends AuthenticationEvent {
  final String? token;
  const CheckAuthEvent({this.token});
}

class LogOutEvent extends AuthenticationEvent {}
