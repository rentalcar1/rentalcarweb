part of 'authentication_bloc.dart';

enum AuthenticationStatus { checking, authenticated, notAuthenticated }

class AuthenticationState extends Equatable {
  final AuthenticationStatus status;
  final String? token;

  const AuthenticationState({required this.status, this.token});

  AuthenticationState copyWith(
          {required AuthenticationStatus status, String? token}) =>
      AuthenticationState(status: status, token: token ?? this.token);
  @override
  List<Object?> get props => [status, token];
}
