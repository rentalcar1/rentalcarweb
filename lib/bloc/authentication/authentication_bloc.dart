import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rantalcarweb/api/rentalCarApi.dart';
import 'package:rantalcarweb/src/shared/ui/service/local_storage.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc()
      : super(
            const AuthenticationState(status: AuthenticationStatus.checking)) {
    on<CheckAuthEvent>(_checkAuthEvent);
    on<LogOutEvent>(_logOutEvent);
  }

  void _checkAuthEvent(
      CheckAuthEvent event, Emitter<AuthenticationState> emit) {
    print('auth: ${LocalStorage.prefs.getString('token')}');
    emit(state.copyWith(status: AuthenticationStatus.checking));
    Future.delayed(const Duration(seconds: 3));
    if (LocalStorage.prefs.getString('token') != null) {
      emit(state.copyWith(
          status: AuthenticationStatus.authenticated,
          token: LocalStorage.prefs.getString('token')));
    } else {
      emit(state.copyWith(status: AuthenticationStatus.notAuthenticated));
    }
  }

  void _logOutEvent(
      LogOutEvent event, Emitter<AuthenticationState> emit) async {
    LocalStorage.prefs.setString('token', '');
    emit(state.copyWith(
        status: AuthenticationStatus.notAuthenticated, token: ''));
  }
}
